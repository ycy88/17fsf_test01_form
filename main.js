const express = require('express');

var app = express();

// routing
app.use(express.static(__dirname + '/public'));
app.use("/libs", express.static(__dirname + "/bower_components"));

app.get('/request', function(req, res){
  res.status(200);
  res.type('application/json');
  res.json({ 
      "result" : "Success! You have registered with us.",
  });
});


// ports and listen 
var port = parseInt(process.argv[2]) || 3000;

app.listen(port, function(){
  console.log("Now listening on port %d", port);
});