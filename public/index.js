(function(){
  angular
    .module("RegApp", [])
    .controller("RegController", RegController)
  
  RegController.$inject = [ "$http" ];

  function RegController($http){
    var regC = this;

    // control vars 
    regC.verified = false;
    regC.qualify = false;
    regC.date = new Date();

    // setup
    regC.initialize = function() {
      regC.username = "";
      regC.email = "";
      regC.password = "";
      regC.gender = "male";
      regC.address = "";
      regC.country = "";
      regC.contact = "";
    };

    // initialize
    regC.initialize();
    regC.status = false; // status of registration
    regC.result = "";


    // functions
    regC.checkBirthday = function() {
      var ageDiff = Date.now() - regC.date.getTime();
      var ageDate = new Date(ageDiff);
      var diffYears = Math.abs(ageDate.getUTCFullYear() - 1970);
      // confirm they have verified
      regC.verified = true;
      regC.qualify = (diffYears >= 18);
    };

    regC.submitForm = function(){
      var p = $http.get('/request', {
        params: {
          "birthday" : regC.date,
          "name" : regC.username,
          "email" : regC.email,
          "password" : regC.password,
          "gender" : regC.gender, 
          "address" : regC.address,
          "country" : regC.country,
          "contact" : regC.contact 
        }
      });
      // success 200 
      p.then(function(result){
        regC.result = result.data.result;
        regC.status = true;
      });
      // failure 400 
      p.catch(function(result){
        regC.result = "Failure! Our servers seemt to have a problem. Please try again."
        regC.status = true;
      });
    }// close submit 

  }// close controller 

})();